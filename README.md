# linkedScripts

## Disclamer
Le développeur initial de ce script est https://gist.github.com/thealphadollar
j'ai fait de légères modifications pour l'adapter au linkedin français


## Getting started
- [ ] Lancez une recherche linkedin sur le type de personne avec qui vous souhaitez vous connecter. Par exemple "Recruteur tech", filtrez sur "personne 2nd et 3eme" ajoutez les filtres que vous voulez.
- [ ] Ouvrez une console dans votre inspecteur chrome, firefox ou autre. (Touche F12 généralement)

## Preparation

- [ ] [actionDelay] Durée en miliseconde entre les actions. Je ne conseille pas de mettre moins de 1000 (1 seconde) pour éviter un blocage
==> Testez avec 4000 ou plus pour voir si tout se passe bien au premier envoi.
- [ ] [addNote] true ou false, selon si vous souhaitez envoyer un message perso
- [ ] [note] Message générique à envoyer. J'ajouterai plus tard une option pour insérer le nom
- [ ] [maxRequests] Nombre d'invitation qui seront envoyées par le script.


## Disclaimer 2

Vous êtes l'unique responsable de l'utilisation que vous en faites.
Si vous abusez et que votre compte est temporairement bloqué, je ne pourrai pas être tenu responsable.
(Et si vous n'êtes pas un sauvage ça devrait bien se passer ^^)


***
